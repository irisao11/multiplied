package com.sda.multiply.matrix;

import java.util.Scanner;

public class CreateMatrix {

    private int row;
    private int col;

    //constructor for a matrix
    public CreateMatrix(int row, int col) {
        this.row = row;
        this.col = col;
    }
        //creating a matrix with the two class parameters
    public int[][] createMatrix() {

        //   Scanner s = new Scanner(System.in);
        int[][] newMatrix = new int[row][col];
        int newElem = 0;
        System.out.println("the matrix has " + row * col + " elements");
        for (int i = 0; i < newMatrix.length; i++) {
            for (int j = 0; j < newMatrix[i].length; j++) {
                newMatrix[i][j] = newElem + 1;
                newElem = newElem + 1;
            }
        }
        System.out.println("The Matrix is: ");
        for (int i = 0; i < newMatrix.length; i++) {
            for (int j = 0; j < newMatrix[i].length; j++) {
                System.out.print(newMatrix[i][j] + " ");
            }
            System.out.println();
        }
        return newMatrix;
    }
}
