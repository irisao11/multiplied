package com.sda.multiply.matrix;

// class to solve multiplication of two matrix
public class MultiplyMatrix {

    //method to solve multiplication of two matrix.
    //has three parameters, because we can only multiply two matrix with
    //same length of first col, second row
    public int[][] multiplyMatrix(int row1, int col1row2, int col2) {

        //construct the two matrix and the multiplied matrix
        CreateMatrix createMatrix1 = new CreateMatrix(row1, col1row2);
        int[][] matrixOne = createMatrix1.createMatrix();

        CreateMatrix createMatrix2 = new CreateMatrix(col1row2, col2);
        int[][] matrixTwo = createMatrix2.createMatrix();

        int[][] multipliedMatrix = new int[row1][col2];
        //loop to multiply matrix
        for (int i = 0; i < row1; i++) {
            for (int j = 0; j < col2; j++) {
                for (int k = 0; k < col1row2; k++) {
                    multipliedMatrix[i][j] += matrixOne[i][k] * matrixTwo[k][j];
                }
            }
        }

        System.out.println("the multiplied Matrix is: ");

        //loop to display multiplied matrix
        for (int[] row : multipliedMatrix) {
            for (int col : row) {
                System.out.print(col + "  ");
            }
            System.out.println();
        }
        return multipliedMatrix;
    }
}
