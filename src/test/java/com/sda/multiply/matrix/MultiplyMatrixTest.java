package com.sda.multiply.matrix;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class MultiplyMatrixTest {
    //testing the method multiplyMatrix
    @Test
    public void multiplyMatrixTest() {

        int[][] multipliedMatrixTest = {{22, 28}, {49, 64}};
        MultiplyMatrix multiplyMatrix = new MultiplyMatrix();

        int[][] myResult = multiplyMatrix.multiplyMatrix(2, 3, 2);

        Assert.assertArrayEquals(multipliedMatrixTest, myResult);
    }

}
